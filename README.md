# Specyfikacja funkcjonalna podsystemu obsługi pojazdów nowych

## 1. Cel projektu
Celem projektu jest stworzenie dokumentacji dla części sytemu CEPIK w zakresie obsługi ewidencji pojazdów nowych. Zakresem systemu jaki obejmuje ten proejekt jest moduł związany z obsługą ewidencji pojazdów nowych.

## 2. Wykaz wymagań kontraktowych

- automatyczne przyjmowanie, weryfikacja i ewidencja informacji od producenta/importera pojazdów dotyczących nowo wystawionych kart pojazdów,
- wyszczególnienie pojazdów nowych, ale jeszcze niezarejestrowanych,
- przesyłanie informacji o stwierdzonych niezgodnościach do producenta/importera,
- obsługa ewidencji podmiotów uczestniczących w obrocie pojazdami (organ rejestrujący lub cofający rejestrację pojazdu – za pośrednictwem wydziałów komunikacji starostw)

## 3. Przyjęte założenia i ograniczenia

Przyjmujemy założenie, że producent/importer pojazdów umieszcza dane w postaci kart pojazdów w składnicy danych. Co pewien, określony czas dane są pobierane ze składnicy danych przez system CEPIK.

Pliki tworzone są od razu po wyprodukowaniu pojazdu. W związku z tym nie znany jest jeszcze właściciel pojazdu, a co za tym idzie jedynymi informacjami wprowadzanymi przez importera/producenta będą:
- dane identyfikacyjne pojazdu,
- parametry techniczne pojazdu.

Pojazd automatycznie zostanie oznaczony jako niezarejestrowany.

System nie powinien przyjąć zgłoszenia w przypadku, gdy któraś z danych identyfikacyjnych pojazdu oraz parametrów technicznych pojazdu nie zostanie wprowadzona.

Weryfikacja wprowadzonych informacji będzie odbywać się przez sprawdzenie, czy wprowadzony numer VIN nie znajduje się już w systemie.

System na bieżąco informuje o wykrytych niezgodnościach i wysyła powiadomienie do producenta/importera.

## 4. Odwzorowanie wymagań kontraktowych na wymagania systemowe

| Nr wymagania kontraktowego | Nr wymagania systemowego | Nazwa wymagania systemowego |
|:--------------------------:|:------------------------:|:---------------------------:|
| 1                          | 1                        | Importowanie danych kart pojazdów do systemu |
| 1                          | 2                        | Weryfikacja poprawności danych               |
| 1                          | 3                        | Ewidencjonowanie importowanych danych w systemie |
| 2                          | 4                        | Wyszczególnienie pojazdów nowych, ale jeszcze niezarejestrowanych |
| 4                          | 6                        | Wyświetlenie powiadomień o zgodności/niezgodności danych |
| 3                          | 5                        | Przesłanie danych |

## 5. Opis wymagań systemowych

Nr wymagania systemowego: 1
Opis: System co pewien, określony czas sprawdza, czy w zewnętrznej składnicy danych znajdują się nowe dane. Następnie, w przypadku wykrycia nowych danych importuje je do systemu CEPIK.

Nr wymagania systemowego: 2
Opis: System weryfikuje poprawność danych przez sprawdzenie czy wprowadzone dane są kompletne oraz, czy wprowadzony numer VIN nie znajduje się już w systemie.

Nr wymagania systemowego: 3
Opis: System przetwarza dane zaimportowane z pliku i utrwala je w systemie (w bazie danych).

Nr wymagania systemowego: 4
Opis: System przy każdym z pojazdów umożliwia oznaczenie go jako zarejestrowany (nowy).

Nr wymagania systemowego: 5
Opis: System wyświetla powiadomienie o poprawności danych lub niepoprawności danych, w którym informuje, które karty produktów są niekompletne oraz, które dane system uznał za niepoprawne

Nr wymagania systemowego: 6
Opis: System umożliwia udostępnienie kart pojazdów na żądanie odpowiednich organów

## 6. Diagram kontekstowy (ang. _data flow diagram_)

![data_flow_diagram.jpg](data_flow_diagram.jpg)

## 7. Diagram procesów (ang. _detailed data flow diagram_)

![detailed_data_flow_diagram.png](detailed_data_flow_diagram.png)